# 3S
Sigmal Server Spin 

## Description
To automate the process of spinning up servers.
Some branches are specifically for the server.


## Branch Maps
|  **Branch**  |  Description
| :-----: | :----
| Challenges | A set of challenges of all types. 
| Website | Sigmal Website
| Spin_drop | Spins a default ubuntu droplet (no configurations)
| Infrastructure | Sets up all the commands needed to run any potential commands needed. 
| Minecraft | Spins a droplet for a minecraft server.  

## How to use this Repository
* Select a branch and check if that is the code you want. 
* To clone (take) from a certain branch:

> `git clone -b <branch_name> <remote-repo-url>`

## Notes
* Do not try to merge the branches
* If you push/pull to a branch, create a branch:
> `git branch <branch>`

And then do:

> `git pull origin <branch>`

* .toml files are meant to be config files. 